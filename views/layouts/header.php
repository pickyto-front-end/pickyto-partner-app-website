<?php
use yii\helpers\Html;
use app\components\utils\UserDetails;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

$ordersBG = '';
$itemsBG = '';
$storeBG = '';

if (Yii::$app->controller->id == "orders"){
    $ordersBG = "cadetblue";
}

if (Yii::$app->controller->id == "items"){
    $itemsBG = "cadetblue";
}

if (Yii::$app->controller->id == "store"){
    $storeBG = "cadetblue";
}

?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">
            <img src="'.\Yii::$app->request->BaseUrl .'/img/PickytoPartnerApp-Logo.jpg" class="img-bordered"  alt="Logo Image"/></span>
            <span class="logo-lg"><img src="'.\Yii::$app->request->BaseUrl .'/img/PickytoPartnerApp-Logo.jpg" class="img-bordered"  alt="Logo Image"/></span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav" style="font-style: 18px; font-weight: bolder; width: 100%; margin-right: 10%;">

                <li class="font-weight-bolder" style="background-color: <?= $ordersBG ?>" >
                <a class="nav-link" href="<?= Url::toRoute(['orders/index']) ?>">
                    <i class="fa fa-arrow-circle-right"></i>
                    Orders
                 </a>
                </li>

                <li class="font-weight-bolder" style="background-color: <?= $itemsBG ?>">
                <a class="nav-link" href="<?= Url::toRoute(['items/index']) ?>">
                    <i class="fa fa-arrow-circle-right"></i>
                    Items
                 </a>
                </li>

                
                <li class="font-weight-bolder" style="background-color: <?= $storeBG ?>">
                <a class="nav-link" href="<?= Url::toRoute([
                                            'store/view', 
                                            'id' => UserDetails::get_restaurant_id(),
                                            ])
                                        ?> 
                                        ">
                    <i class="fa fa-arrow-circle-right"></i>
                    Store
                 </a>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= $directoryAsset ?>/img/avatar5.png" class="user-image" alt="User Image"/>
                        <span class="hidden-xs"><?= Yii::$app->user->identity->getFullName() ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?= $directoryAsset ?>/img/avatar5.png" class="img-circle"
                                 alt="User Image"/>

                            <p>
                            <?= Yii::$app->user->identity->getFullName() ?>
                            <small>Pickyto Preferred Partner</small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body">
                            <div class="col-xs-4 text-center">
                                <a href="#">Account</a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="#">Settings</a>
                            </div>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            
                            <div class="pull-right">
                                <?= Html::a(
                                    'Sign out',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
    </nav>
</header>
