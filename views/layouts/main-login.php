<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;;

/* @var $this \yii\web\View */
/* @var $content string */

dmstr\web\AdminLteAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width">
    <meta name="theme-color" content="#ffffff">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

   
    <link rel="apple-touch-icon" sizes="57x57" href="<?= Url::to('@web/img/apple-icon-57x57.png') ?> ">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= Url::to('@web/img/apple-icon-60x60.png') ?> ">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= Url::to('@web/img/apple-icon-72x72.png') ?> ">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= Url::to('@web/img/apple-icon-120x120.png') ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= Url::to('@web/img/apple-icon-144x144.png') ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= Url::to('@web/img/apple-icon-180x180.png') ?>">


    <link rel="icon" type="image/png" sizes="36x36"  href="<?= Url::to('@web/img/android-icon-36x36.png') ?> ">
    <link rel="icon" type="image/png" sizes="48x48"  href="<?= Url::to('@web/img/android-icon-48x48.png') ?> ">
    <link rel="icon" type="image/png" sizes="72x72"  href="<?= Url::to('@web/img/android-icon-72x72.png') ?> ">
    <link rel="icon" type="image/png" sizes="96x96"  href="<?= Url::to('@web/img/android-icon-96x96.png') ?> ">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?= Url::to('@web/img/android-icon-192x192.png') ?> ">


    <link rel="icon" type="image/png" sizes="32x32" href="<?= Url::to('@web/img/favicon-32x32.png') ?> ">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= Url::to('@web/img/favicon-96x96.png') ?> ">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= Url::to('@web/img/favicon-16x16.png') ?> ">

    <link rel="manifest" href="/manifest.json">

    <link rel="icon" type="image/png" sizes="512x512" href="<?= Url::to('@web/img/PartnerAppSplash.png') ?> ">

    <?php $this->head() ?>
</head>
<body class="login-page" style="background-color: peachpuff;">

<?php $this->beginBody() ?>

    <?= $content ?>

<?php $this->endBody() ?>
</body>

<script>
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('/js/sw.js').then(() => {
            console.log('Service Worker registered successfully.');
        }).catch(error => {
            console.log('Service Worker registration failed:', error);
        });
    }
</script>

</html>
<?php $this->endPage() ?>
