<?php
use yii\helpers\Html;
use app\components\utils\UserDetails;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <nav class="navbar navbar-static-top"  style="background-color:cadetblue;" role="navigation">

        <div class="navbar-custom-menu" style="align-items: center;font-weight: bolder; margin-right: 20%; ">

            <ul class="nav navbar-nav">

                <li class="font-weight-bolder">
                <a class="nav-link" href="<?= Url::toRoute(['orders/index']) ?>">
                    <i class="fa fa-arrow-circle-right"></i>
                    Pending
                 </a>
                </li>

                <li class="">
                <a class="nav-link" href="<?= Url::toRoute(['orders/accepted-orders']) ?>">
                    <i class="fa fa-arrow-circle-right"></i>
                    Accepted
                 </a>
                </li>

                
                <li class="">
                    <a class="nav-link" href="<?= Url::toRoute([ 'orders/ready-orders', 'id' => UserDetails::get_restaurant_id()]) ?>" > 
                    <i class="fa fa-arrow-circle-right"></i>
                    
                    Ready
                 </a>
                </li>

            </ul>
        </div>
    </nav>
</header>