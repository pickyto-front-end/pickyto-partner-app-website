<?php

use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RestaurantsModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="restaurants-model-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'layout' => 'horizontal',
    ]); ?>

    <?= $form->field($model, 'restaurant_name')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'is_open_today')->dropDownList(['Y' => 'Y', 'N' => 'N'], ['options' =>  [$model->is_open_today => ['selected' => "true"]]  ]) ?>

    <br>
    <hr>
    <br>


    <div class="row">
    
        <span class="col-sm-3 text-bold text-xl-center">Logo Image:&nbsp;&nbsp;&nbsp;</span>

        <span class="col-sm-3"><img src="<?= $model->logo_img_url ?>" width="100px" height="100x" /></span>

    </div>

    <div style="height: 20px;"> </div>

    <div class="row">

        <span class="col-sm-3 text-bold text-xl-center">Banner Image:</span>
        
        <span class="col-sm-3"><img src="<?= $model->banner_img_url ?>" width="100px" height="100x" /></span>

    </div>

    <br>
    
    <hr>

    <br>

    <?= $form->field($model, 'company_legal_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fssai_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'company_tax_id')->textInput(['maxlength' => true]) ?>

    <br>
    <hr>
    <br>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'locality')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'state')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'primary_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'secondary_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'primary_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'secondary_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'branch_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'latitude')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'longitude')->textInput(['maxlength' => true]) ?>

    
    <br>
    <hr>
    <br>

    <?= $form->field($model, 'currency_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bank_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bank_account_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bank_account_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'IFSC')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bank_branch')->textInput(['maxlength' => true]) ?>


    <br>
    <hr>
    <br>

    <?= $form->field($model, 'last_modified_date')->hiddenInput(['value' => time()])->label(false) ?>

    <?= $form->field($model, 'last_updated_by')->hiddenInput(['value' => Yii::$app->user->identity->getId() ])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('Save & Update', ['class' => 'btn btn-lg btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
