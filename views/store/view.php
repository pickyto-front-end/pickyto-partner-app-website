<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RestaurantsModel */


?>

<div class="restaurants-model-view">

    <div class="row">
        <div class="col-sm-4">
            <h3><?=  $model->restaurant_name ?></h3>
        </div>
        
        <div class="col-sm-4">
             <?= Html::a('Update', ['update', 'id' => $model->restaurant_id], ['class' => 'btn btn-lg btn-info ']) ?>
        </div>

        
    </div>


    <br>
    <hr>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'restaurant_id',
            'restaurant_name',
            [
                'attribute' => 'Logo Image',
                'value' => $model->logo_img_url,
                'format' => ['image',['width'=>'100','height'=>'100']],
            ],
            [
                'attribute' => 'Banner Image',
                'value' => $model->banner_img_url,
                'format' => ['image',['width'=>'100','height'=>'100']],
            ],
            'restaurant_status',
            'is_open_today',
            'delivery_range',
        ],
        ]) ?>
            
        <br>
        <hr>
        <br>


        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            'fssai_number',
            'company_legal_id',
            'company_tax_id',
        ],
        ]) ?>

        <br>
        <hr>
        <br>

        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            'address',
            'locality',
            'city',
            'state',
            'country',
            'branch_name',
            'latitude',
            'longitude',
            'primary_phone',
            'secondary_phone',
            'primary_email:email',
            'secondary_email:email',
        ],
        ]) ?>   

        <br>
        <hr>
        <br>

        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'commission_pct',
            'currency_code',
            'bank_name',
            'bank_account_no',
            'bank_account_name',
            'IFSC',
            'bank_branch',
            
            'created_date:datetime',
            
        ],
    ]) ?>

<br><br>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->restaurant_id], ['class' => 'btn btn-lg btn-info ']) ?>
    </p>

</div>
