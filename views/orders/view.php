<?php

use app\components\types\OrderStatusEnum;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerOrdersModel */

$this->title = "Details of Order Id:  ".$model->order_id;
$this->params['breadcrumbs'][] = ['label' => 'Order Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

\yii\web\YiiAsset::register($this);

?>
<div class="customer-orders-model-view">    

    <h2> Items List: </h2>

    <?= GridView::widget([
        'dataProvider' => $orderItems,
        'columns' => [
            'item_id',
            'item_name',
            'item_unit_price',
            'quantity',
        ],
    ]); ?>

    <h2> Order Details: </h2>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'order_id',
            'create_date:datetime',
            'order_status',
            'customer_name',
            'phone',
            'discounted_order_amount',
            'tax_amount',
            'parcel_charge',
            'delivery_charge',
            'order_amount',
        ],
    ]) ?>


    <br>
    <br>
    

    <?php 

    $yesButton = '';

    $noButton = Html::a('Reject Order', ['reject-order', 'orderId' => $model->order_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to Reject this Order?',
                    'method' => 'post',
                ]]);
    
    
    if (strcmp($model->order_status, OrderStatusEnum::PAYMENT_COMPLETED) ==0) {
    
        $yesButton =  Html::a('Accept Order', ['accept-order', 'orderId' => $model->order_id], ['class' => 'btn btn-success']);


     } elseif (strcmp($model->order_status, OrderStatusEnum::RESTAURANT_CONFIRMED) ==0)  {

        $yesButton =  Html::a('Mark Ready', ['mark-ready', 'orderId' => $model->order_id], ['class' => 'btn btn-success']);

     } elseif (strcmp($model->order_status, OrderStatusEnum::PENDING_PICKUP) ==0) {

        $yesButton =  Html::a('Mark Delivered', ['mark-delivered', 'orderId' => $model->order_id], ['class' => 'btn btn-success']);

        $noButton ='';

     } elseif (strcmp($model->order_status, OrderStatusEnum::ORDER_DELIVERED) ==0) {

        $yesButton = '<h3>~~ All Done ~~ <br> No More Action To Take</h3>';

        $noButton = '';

     }  else {

        $yesButton = '<h3>Unknow Order Status. Contact Support hello@pickyto.com / +91-7397-306-444</h3>';

        $noButton = '';
     }
     ?>

    <p>
        
        <?= $yesButton ?>

        &nbsp;&nbsp;&nbsp;

        <?= $noButton ?>

    </p>



</div>
