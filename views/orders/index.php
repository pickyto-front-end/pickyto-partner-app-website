<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

?> 

<div class="container-fluid">
<div class="row" >
    
    <?php foreach ($paymentCompletedOrders as $paymentCompletedOrder) { ?>
        <div class="col-sm-4">
        <div class="box">
            <div class="box-header bg-black-gradient" >
                <h3 class="box-title">Order Id: <?php echo $paymentCompletedOrder->order_id ?></h3>
            </div>
            <!-- div:box-header -->

            <div class="box-body mx-5">
                <strong>Order Status: </strong>
                <em><?php echo $paymentCompletedOrder->order_status; ?></em>
                
                <br>

                <strong> Type: </strong>
                <em>Delivery</em>
                
                <br>

                <strong class="px-lg-5 mx-5">Customer Name: </strong>
                <em><?= $paymentCompletedOrder->customer_name ?></em>

                <br><br>
        
                <blockquote>
                    <p>
                        <strong>Order Total: </strong>
                        <em> <?= $paymentCompletedOrder->discounted_order_amount ?> </em>

                    </p>
                </blockquote>

            </div>
            <!-- div: box-body -->

            <div class="box-footer text-center" style="border: saddlebrown thin dotted;">
                 <?= Html::a('Accept / Confirm Order', ['accept-order', 'orderId' => $paymentCompletedOrder->order_id], ['class' => 'btn btn-success text-bold']) ?>

                 <?= Html::a('View', ['view', 'id' => $paymentCompletedOrder->order_id], ['class' => 'btn btn-warning text-bold']) ?>
            
            </div>

        </div>
        <!-- div:box -->
        </div><!-- div: col-sm-4 -->
    <?php } ?>


</div>
<!-- .div-row -->
</div>
<!-- div:container -->

