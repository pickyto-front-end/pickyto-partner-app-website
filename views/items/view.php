<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ItemDetailsModel */

$this->title = $model->item_id;
$this->params['breadcrumbs'][] = ['label' => 'Item Details Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="item-details-model-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->item_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->item_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'item_id',
            'product_code',
            'restaurant_id',
            'item_name',
            'item_desc',
            'primary_img',
            'primary_cuisine',
            'primary_category',
            'sub_category',
            'price',
            'is_tax_inclusive',
            'item_parcel_charge',
            'is_parcel_tax_applicable',
            'item_status',
            'is_refill_item',
            'is_veg',
            'is_customizable',
            'is_todays_special',
            'serving_time_slots',
            'serving_times',
            'available_days',
            'prep_time:datetime',
            'item_tax_code',
            'printer_config_name',
            'img_url_1:url',
            'img_url_2:url',
            'img_url_3:url',
            'tag_1',
            'tag_2',
            'tag_3',
            'item_count',
            'alert_threshold',
            'meta_data',
            'created_date',
            'last_modified_date',
            'last_updated_by',
            'is_website_displayable',
        ],
    ]) ?>

</div>
