<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url; 
/* @var $this yii\web\View */
/* @var $searchModel app\models\ItemDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="item-details-model-index">

<div class="container-fluid">
    <div class="row">
        <i class="fa fa-arrow-circle-right"></i>

        <a class="text-black font-weight-bolder"  href="<?= Url::toRoute(['items/index']) ?>" >
            <div class="btn btn-md btn-success">            
                    Category
            </div>
        </a>

        <i class="fa fa-arrow-circle-right"></i>
        
        <div class="btn btn-md btn-info">    Sub-Category     </div>

        <i class="fa fa-arrow-circle-right"></i>

        <div class="btn btn-sm btn-default disabled">      Items   </div>

    </div>
</div>

<hr>

<div class="container-fluid">
    <div class="row" >
        <?php foreach ($subCats as $subCategory) { ?>

        
        <div class="col-sm-3">
            <a href="<?= Url::toRoute(['items/view-items-by-sub-cats', 'subCategoryId' => $subCategory->sub_category_id, 
                                        'categoryId' => $subCategory->primary_category_id]) ?>" >

            <div class="box btn btn-lg bg-white text-center" style="border: thin dotted black;">
                <div class="box-header">  
                
                    <h3 class="text-white"><?php echo $subCategory->sub_category_name ?></h3>
            
                </div> 
            </div>
            </a>
        </div> <!-- div:col-sm-4 -->

        <?php } ?>

    </div> <!-- div:row -->
</div><!-- div:container -->


</div>