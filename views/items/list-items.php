<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url; 
/* @var $this yii\web\View */
/* @var $searchModel app\models\ItemDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$primaryCategoryid = $_GET['categoryId']

?>
<div class="item-details-model-index">

<div class="container-fluid">
    <div class="row">
        <i class="fa fa-arrow-circle-right"></i>
        <div class="btn btn-md btn-success">            
            <a class="text-black font-weight-bolder"  href="<?= Url::toRoute(['items/index']) ?>" >
                Category
            </a>
        </div>

        <i class="fa fa-arrow-circle-right"></i>

        <div class="btn btn-md btn-success">
            <a class="text-black font-weight-bolder"  href="<?= Url::toRoute(['items/view-sub-cats', 'categoryId' => $primaryCategoryid]) ?>" >
                Sub-Category
            </a>
        </div>

        <i class="fa fa-arrow-circle-right"></i>

        <div class="btn btn-md btn-info">
            <a class="text-black font-weight-bolder"  href="#" >
                Items
            </a>
        </div>

    </div>
</div>

<hr>

<div class="container-fluid">
    <div class="row" >
        <?php foreach ($storeItems as $storeItem) { 
           $boxBorder = "brown";

            if ($storeItem->item_status == "AVAILABLE") {
                $boxBorder = "green";
            }
            
            ?>

        <div class="col-sm-3">
            <a href="<?= Url::toRoute(['items/update', 'itemId' => $storeItem->item_id, 'itemStoreId' => $storeItem->item_store_mapping_id]) ?>" >
            <div class="box btn btn-lg bg-white text-center" style="border: thick solid <?= $boxBorder ?>;">
                <div class="">  
                    <img src="<?= $masterItems[$storeItem->item_id]->primary_img ?>" width="50px" height="50px" />
                    <h3 class="text-white">
                        <?php 
                         $itemName = $masterItems[$storeItem->item_id]->item_name;
                         $price = $storeItem->sale_price;
                         
                         echo $itemName;
                         echo '<br>';
                         echo '<span style="color: brown;">';
                         echo '&#8377;'.$price;
                         echo '</span>';
                         echo '<br>';
                         echo '<small><em>[edit]</em></small>';
                         ?></h3>
            
                </div> 
            </div>
            </a>
        </div> <!-- div:col-sm-4 -->

        <?php } ?>

    </div> <!-- div:row -->
</div><!-- div:container -->


</div>