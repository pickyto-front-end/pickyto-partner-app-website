<?php

use app\components\utils\UserDetails;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ItemDetailsModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-details-model-form">

    <?php $form = ActiveForm::begin(); ?>

   <div class="col">
       <img src="<?= $masterItem->primary_img ?>" width="100px" height="100x" />
       <span class="font-weight-bolder" style="font-size: 18px; font-weight: bolder;"> <?= $masterItem->item_name ?> </span>
    </div>

    <br>

    <hr>   

    <?= $form->field($masterItem, 'item_name')->textInput() ?>
    
    <?= $form->field($model, 'list_price')->textInput() ?>

    <?= $form->field($model, 'sale_price')->textInput() ?>

    <?=
        $form->field($masterItem, 'is_veg')->dropDownList(['Y' => 'Is Veg', 'N' => 'Non-Veg'], ['options' =>  [$masterItem->is_veg => ['selected' => "true"]]
           
        ])
    ?>

    <?=
        $form->field($model, 'item_status')
                                ->dropDownList(['AVAILABLE' => 'AVAILABLE', 'PENDING' => 'PENDING', 'UNAVAILABLE' => 'UNAVAILABLE'], 
                                ['options' =>  [$model->item_status => ['selected' => "true"]]
           
        ])
    ?>
    <?= $form->field($model, 'last_modified_date')->hiddenInput(['value' => time()])->label(false) ?>

    <?= $form->field($model, 'last_updated_by')->hiddenInput(['value' => Yii::$app->user->identity->getId() ])->label(false) ?>

    <br><br>
    
    <div class="form-group">
        <?= Html::submitButton('Save & Update', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
