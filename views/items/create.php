<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ItemDetailsModel */

$this->title = 'Create Item Details Model';
$this->params['breadcrumbs'][] = ['label' => 'Item Details Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-details-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
