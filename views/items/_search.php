<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ItemDetailsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-details-model-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'item_id') ?>

    <?= $form->field($model, 'product_code') ?>

    <?= $form->field($model, 'restaurant_id') ?>

    <?= $form->field($model, 'item_name') ?>

    <?= $form->field($model, 'item_desc') ?>

    <?php // echo $form->field($model, 'primary_img') ?>

    <?php // echo $form->field($model, 'primary_cuisine') ?>

    <?php // echo $form->field($model, 'primary_category') ?>

    <?php // echo $form->field($model, 'sub_category') ?>

    <?php // echo $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'is_tax_inclusive') ?>

    <?php // echo $form->field($model, 'item_parcel_charge') ?>

    <?php // echo $form->field($model, 'is_parcel_tax_applicable') ?>

    <?php // echo $form->field($model, 'item_status') ?>

    <?php // echo $form->field($model, 'is_refill_item') ?>

    <?php // echo $form->field($model, 'is_veg') ?>

    <?php // echo $form->field($model, 'is_customizable') ?>

    <?php // echo $form->field($model, 'is_todays_special') ?>

    <?php // echo $form->field($model, 'serving_time_slots') ?>

    <?php // echo $form->field($model, 'serving_times') ?>

    <?php // echo $form->field($model, 'available_days') ?>

    <?php // echo $form->field($model, 'prep_time') ?>

    <?php // echo $form->field($model, 'item_tax_code') ?>

    <?php // echo $form->field($model, 'printer_config_name') ?>

    <?php // echo $form->field($model, 'img_url_1') ?>

    <?php // echo $form->field($model, 'img_url_2') ?>

    <?php // echo $form->field($model, 'img_url_3') ?>

    <?php // echo $form->field($model, 'tag_1') ?>

    <?php // echo $form->field($model, 'tag_2') ?>

    <?php // echo $form->field($model, 'tag_3') ?>

    <?php // echo $form->field($model, 'item_count') ?>

    <?php // echo $form->field($model, 'alert_threshold') ?>

    <?php // echo $form->field($model, 'meta_data') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'last_modified_date') ?>

    <?php // echo $form->field($model, 'last_updated_by') ?>

    <?php // echo $form->field($model, 'is_website_displayable') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
