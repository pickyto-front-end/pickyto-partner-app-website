console.log('Hello from service-worker.js');

const cacheName = 'pickyto-partner-app';

const staticAssets = [
  '../css/site.css',
  '../img/favicon.ico',
];

self.addEventListener('install', async event => {
    console.log('install event')
    const cache = await caches.open(cacheName); 
    await cache.addAll(staticAssets); 
  });

  self.addEventListener('activate', async event => {
  // Perform some task
  console.log('Service worker activating...');
});
  
self.addEventListener('fetch', async event => {
    console.log('fetch event')
  });