<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=' . getenv('STORES_DB_HOST_NAME') . ';dbname=PICKYTO_STORES_DB;port=' . getenv('DB_PORT'),
    'username' => getenv('STORES_DB_USER_NAME'),
    'password' => getenv('STORES_DB_PASSWORD'),
    'charset' => 'utf8',
    'enableSchemaCache' => false,
    'schemaCacheDuration' => 60,
    'schemaCache' => 'cache',
];
?>