<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=' . getenv('USER_DB_HOST_NAME') . ';dbname=LASSI_USERS_DB;port=' . getenv('DB_PORT'),
    'username' => getenv('USER_DB_USER_NAME'),
    'password' => getenv('USER_DB_PASSWORD'),
    'charset' => 'utf8',
    'enableSchemaCache' => false,
    'schemaCacheDuration' => 60,
    'schemaCache' => 'cache',
];
