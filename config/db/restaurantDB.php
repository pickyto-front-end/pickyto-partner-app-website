<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=' . getenv('RESTAURANT_DB_HOST_NAME') . ';dbname=LASSI_RESTAURANT_DB;port=' . getenv('DB_PORT'),
    'username' => getenv('RESTAURANT_DB_USER_NAME'),
    'password' => getenv('RESTAURANT_DB_PASSWORD'),
    'charset' => 'utf8',
    'enableSchemaCache' => false,
    'schemaCacheDuration' => 60,
    'schemaCache' => 'cache',
];
?>