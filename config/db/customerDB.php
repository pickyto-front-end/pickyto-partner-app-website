<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=' . getenv('CUSTOMER_DB_HOST_NAME') . ';dbname=LASSI_CUSTOMER_DB;port=' . getenv('DB_PORT'),
    'username' => getenv('CUSTOMER_DB_USER_NAME'),
    'password' => getenv('CUSTOMER_DB_PASSWORD'),
    'charset' => 'utf8',
    'enableSchemaCache' => false,
    'schemaCacheDuration' => 60,
    'schemaCache' => 'cache',
];
