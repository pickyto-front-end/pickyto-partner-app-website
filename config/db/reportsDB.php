<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=' . getenv('REPORTS_DB_HOST_NAME') . ';dbname=LASSI_REPORTS_DB',
    'username' => getenv('REPORTS_DB_USER_NAME'),
    'password' => getenv('REPORTS_DB_PASSWORD'),
    'charset' => 'utf8',
    'enableSchemaCache' => true,
    'schemaCacheDuration' => 60,
    'schemaCache' => 'cache',
];
