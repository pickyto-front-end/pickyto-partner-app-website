<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=' . getenv('PURCHASE_DB_HOST_NAME') . ';dbname=LASSI_PURCHASE_DB;port=' . getenv('DB_PORT'),
    'username' => getenv('PURCHASE_DB_USER_NAME'),
    'password' => getenv('PURCHASE_DB_PASSWORD'),
    'charset' => 'utf8',
    'enableSchemaCache' => false,
    'schemaCacheDuration' => 60,
    'schemaCache' => 'cache',
];
