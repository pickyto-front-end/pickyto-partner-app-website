<?php

$params = require __DIR__ . '/params.php';

$restaurantDB = require 'db/restaurantDB.php';
$usersDB = require 'db/usersDB.php';
$purchaseDB = require 'db/purchaseDB.php';
$customerDB = require 'db/customerDB.php';
$storesDB = require 'db/pickytoStoresDB.php';


$config = [
    'id' => 'Pickyto Partner App',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],

    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'k34a7VP1dfddTMCuSU4BUS3Ctq7LH5z02ikao',
        ],
        
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => false,
            'authTimeout' => 120,
        ],
        
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        
        /* DB Configs */
        'db' => $usersDB,
        'db_restaurant' => $restaurantDB,
        'purchaseDB' => $purchaseDB,
        'customerDB' => $customerDB,
        'storesDB' => $storesDB,
        

        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        
    ],
    'params' => $params,
    'on beforeRequest' => function ($event) {
        Yii::$app->layout = Yii::$app->user->isGuest ?
                '@app/views/layouts/main.php' :
                '@app/views/layouts/main.php';
    },
    'as beforeRequest' => [
        //if guest user access site so, redirect to login page.
        'class' => 'yii\filters\AccessControl',
        'rules' => [
            [
                'actions' => ['login', 'error'],
                'allow' => true,
            ],
            [
                'allow' => true,
                'roles' => ['@'],
            ],
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
