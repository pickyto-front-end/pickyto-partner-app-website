<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "restaurants".
 *
 * @property int $restaurant_id
 * @property string|null $restaurant_name
 * @property string|null $fssai_number
 * @property int $group_id
 * @property string|null $restaurant_desc
 * @property string|null $restaurant_address_id
 * @property int|null $max_capacity
 * @property int|null $opening_time
 * @property int|null $closing_time
 * @property int|null $delivery_service_start_time
 * @property int|null $delivery_service_end_time
 * @property string|null $restaurant_timings
 * @property string $operating_days
 * @property string $is_open_today
 * @property string $is_parcel_available
 * @property string|null $is_delivery_service_available
 * @property string $is_convenience_fee_charged
 * @property string|null $parcel_service_availability
 * @property string|null $parking_availability
 * @property string|null $wheelchair_accessible
 * @property string|null $logo_img_url
 * @property string|null $banner_img_url
 * @property string|null $thumbnail_img_url
 * @property string|null $cuisine_type
 * @property string $store_type
 * @property string $restaurant_status
 * @property int|null $commission_pct
 * @property string|null $company_legal_id
 * @property string|null $company_tax_id
 * @property string|null $address
 * @property string|null $locality
 * @property string|null $city
 * @property string|null $state
 * @property string|null $country
 * @property string|null $onboard_status
 * @property string|null $branch_name
 * @property float|null $latitude
 * @property float|null $longitude
 * @property float|null $delivery_range
 * @property int|null $primary_phone
 * @property int|null $secondary_phone
 * @property string|null $primary_email
 * @property string|null $secondary_email
 * @property string $currency_code
 * @property string|null $bank_name
 * @property int|null $bank_account_no
 * @property string|null $bank_account_name
 * @property string|null $IFSC
 * @property string|null $bank_branch
 * @property int|null $gen_prep_time
 * @property int|null $cost_per_person
 * @property int $created_date
 * @property int $last_modified_date
 * @property int $last_updated_by
 * @property string|null $restaurant_referral_code
 * @property string|null $external_account_id
 * @property string|null $legal_name
 * @property string|null $trade_name
 */
class RestaurantsModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'restaurants';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_restaurant');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['group_id', 'created_date', 'last_modified_date', 'last_updated_by'], 'required'],
            [['group_id', 'max_capacity', 'opening_time', 'closing_time', 'delivery_service_start_time', 'delivery_service_end_time', 'commission_pct', 'primary_phone', 'secondary_phone', 'bank_account_no', 'gen_prep_time', 'cost_per_person', 'created_date', 'last_modified_date', 'last_updated_by'], 'integer'],
            [['restaurant_timings'], 'safe'],
            [['latitude', 'longitude', 'delivery_range'], 'number'],
            [['restaurant_name', 'company_legal_id', 'company_tax_id', 'bank_name', 'bank_branch'], 'string', 'max' => 200],
            [['fssai_number'], 'string', 'max' => 20],
            [['restaurant_desc', 'logo_img_url', 'banner_img_url', 'thumbnail_img_url', 'cuisine_type', 'primary_email', 'secondary_email'], 'string', 'max' => 500],
            [['restaurant_address_id'], 'string', 'max' => 250],
            [['operating_days', 'wheelchair_accessible'], 'string', 'max' => 10],
            [['is_open_today', 'is_parcel_available', 'is_delivery_service_available', 'is_convenience_fee_charged', 'parcel_service_availability'], 'string', 'max' => 2],
            [['parking_availability'], 'string', 'max' => 25],
            [['store_type', 'locality', 'city', 'state', 'country', 'onboard_status', 'branch_name', 'bank_account_name', 'IFSC', 'legal_name', 'trade_name'], 'string', 'max' => 100],
            [['restaurant_status'], 'string', 'max' => 50],
            [['address'], 'string', 'max' => 1000],
            [['currency_code', 'restaurant_referral_code', 'external_account_id'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'restaurant_id' => 'Restaurant ID',
            'restaurant_name' => 'Restaurant Name',
            'fssai_number' => 'Fssai Number',
            'group_id' => 'Group ID',
            'restaurant_desc' => 'Restaurant Desc',
            'restaurant_address_id' => 'Restaurant Address ID',
            'max_capacity' => 'Max Capacity',
            'opening_time' => 'Opening Time',
            'closing_time' => 'Closing Time',
            'delivery_service_start_time' => 'Delivery Service Start Time',
            'delivery_service_end_time' => 'Delivery Service End Time',
            'restaurant_timings' => 'Restaurant Timings',
            'operating_days' => 'Operating Days',
            'is_open_today' => 'Is Open Today',
            'is_parcel_available' => 'Is Parcel Available',
            'is_delivery_service_available' => 'Is Delivery Service Available',
            'is_convenience_fee_charged' => 'Is Convenience Fee Charged',
            'parcel_service_availability' => 'Parcel Service Availability',
            'parking_availability' => 'Parking Availability',
            'wheelchair_accessible' => 'Wheelchair Accessible',
            'logo_img_url' => 'Logo Img Url',
            'banner_img_url' => 'Banner Img Url',
            'thumbnail_img_url' => 'Thumbnail Img Url',
            'cuisine_type' => 'Cuisine Type',
            'store_type' => 'Store Type',
            'restaurant_status' => 'Restaurant Status',
            'commission_pct' => 'Commission %',
            'company_legal_id' => 'Legal ID (PAN)',
            'company_tax_id' => 'Tax ID (GST)',
            'address' => 'Address',
            'locality' => 'Locality',
            'city' => 'City',
            'state' => 'State',
            'country' => 'Country',
            'onboard_status' => 'Onboard Status',
            'branch_name' => 'Branch Name',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'delivery_range' => 'Delivery Range',
            'primary_phone' => 'Primary Phone',
            'secondary_phone' => 'Secondary Phone',
            'primary_email' => 'Primary Email',
            'secondary_email' => 'Secondary Email',
            'currency_code' => 'Currency Code',
            'bank_name' => 'Bank Name',
            'bank_account_no' => 'Bank Account No',
            'bank_account_name' => 'Bank Account Name',
            'IFSC' => 'IFSC Code',
            'bank_branch' => 'Bank Branch',
            'gen_prep_time' => 'Gen Prep Time',
            'cost_per_person' => 'Cost Per Person',
            'created_date' => 'Registered Date',
            'last_modified_date' => 'Last Modified Date',
            'last_updated_by' => 'Last Updated By',
            'restaurant_referral_code' => 'Restaurant Referral Code',
            'external_account_id' => 'External Account ID',
            'legal_name' => 'Legal Name',
            'trade_name' => 'Trade Name',
        ];
    }
}
