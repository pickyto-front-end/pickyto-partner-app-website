<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item_sub_categories".
 *
 * @property int $sub_category_id
 * @property int $primary_category_id
 * @property string $sub_category_name
 * @property int|null $created_date
 * @property int|null $last_modified_date
 * @property string|null $last_updated_by
 */
class ItemSubCategoriesModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'item_sub_categories';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('storesDB');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['primary_category_id', 'sub_category_name'], 'required'],
            [['primary_category_id', 'created_date', 'last_modified_date'], 'integer'],
            [['sub_category_name'], 'string', 'max' => 200],
            [['last_updated_by'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sub_category_id' => 'Sub Category ID',
            'primary_category_id' => 'Primary Category ID',
            'sub_category_name' => 'Sub Category Name',
            'created_date' => 'Created Date',
            'last_modified_date' => 'Last Modified Date',
            'last_updated_by' => 'Last Updated By',
        ];
    }
}
