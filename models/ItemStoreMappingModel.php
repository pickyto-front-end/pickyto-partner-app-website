<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item_store_mapping".
 *
 * @property int $item_store_mapping_id
 * @property int $store_id
 * @property int|null $group_id
 * @property int $item_id
 * @property int|null $primary_category_id
 * @property int|null $sub_category_id
 * @property float|null $list_price
 * @property float|null $sale_price
 * @property string|null $item_status
 * @property int $availability_count
 * @property int|null $created_date
 * @property int|null $last_modified_date
 * @property int|null $last_updated_by
 */
class ItemStoreMappingModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'item_store_mapping';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('storesDB');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['store_id', 'item_id', 'availability_count'], 'required'],
            [['store_id', 'group_id', 'item_id', 'primary_category_id', 'sub_category_id', 'availability_count', 'created_date', 'last_modified_date', 'last_updated_by'], 'integer'],
            [['list_price', 'sale_price'], 'number'],
            [['item_status'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'item_store_mapping_id' => 'Item Store Mapping ID',
            'store_id' => 'Store ID',
            'group_id' => 'Group ID',
            'item_id' => 'Item ID',
            'primary_category_id' => 'Primary Category ID',
            'sub_category_id' => 'Sub Category ID',
            'list_price' => 'List Price',
            'sale_price' => 'Sale Price',
            'item_status' => 'Item Status',
            'availability_count' => 'Availability Count',
            'created_date' => 'Created Date',
            'last_modified_date' => 'Last Modified Date',
            'last_updated_by' => 'Last Updated By',
        ];
    }
}
