<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item_categories".
 *
 * @property int $category_id
 * @property string $category_name
 * @property string|null $category_desc
 * @property string|null $img_url
 * @property string $category_type
 * @property string $record_status
 * @property int $created_date
 * @property int $last_modified_date
 */
class ItemCategoriesModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'item_categories';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('storesDB');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_name', 'category_type', 'record_status', 'created_date', 'last_modified_date'], 'required'],
            [['created_date', 'last_modified_date'], 'integer'],
            [['category_name', 'category_desc'], 'string', 'max' => 100],
            [['img_url'], 'string', 'max' => 200],
            [['category_type'], 'string', 'max' => 50],
            [['record_status'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Category ID',
            'category_name' => 'Category Name',
            'category_desc' => 'Category Desc',
            'img_url' => 'Img Url',
            'category_type' => 'Category Type',
            'record_status' => 'Record Status',
            'created_date' => 'Created Date',
            'last_modified_date' => 'Last Modified Date',
        ];
    }
}
