<?php

namespace app\models;

use Yii;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "master_item_details".
 *
 * @property int $item_id
 * @property string $item_name
 * @property string|null $item_desc
 * @property int|null $item_brand_id
 * @property string|null $primary_img
 * @property int $primary_category_id
 * @property int|null $sub_category_id
 * @property float|null $maximum_retail_price
 * @property string $is_tax_inclusive
 * @property string|null $item_tax_code
 * @property string|null $is_customizable
 * @property string|null $is_veg
 * @property string|null $is_refill_item
 * @property string|null $item_type
 * @property float|null $item_weight
 * @property string|null $img_url_1
 * @property string|null $img_url_2
 * @property string|null $img_url_3
 * @property int|null $created_date
 * @property int|null $last_modified_date
 * @property string|null $last_updated_by
 */
class MasterItemDetails extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'master_item_details';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb() {
        return Yii::$app->get('storesDB');
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['item_name', 'primary_category_id'], 'required'],
            [['item_brand_id', 'primary_category_id', 'sub_category_id', 'created_date', 'last_modified_date'], 'integer'],
            [['maximum_retail_price', 'item_weight'], 'number'],
            [['item_name'], 'string', 'max' => 100],
            [['item_desc', 'img_url_1', 'img_url_2', 'img_url_3'], 'string', 'max' => 200],
            [['primary_img'], 'string', 'max' => 150],
            [['is_tax_inclusive', 'is_customizable', 'is_veg', 'is_refill_item'], 'string', 'max' => 2],
            [['item_tax_code', 'item_type'], 'string', 'max' => 50],
            [['last_updated_by'], 'string', 'max' => 45],
        ];
    }

    public function behaviors() {
        return [
            // To Autofill timestamps 
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_date',
                'updatedAtAttribute' => 'last_modified_date',
                'value' => time(),
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'last_updated_by',
                    ActiveRecord::EVENT_BEFORE_INSERT => 'last_updated_by',
                ],
                'value' => empty(Yii::$app->user->identity->person_id) ? 1 : Yii::$app->user->identity->person_id,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'item_id' => 'Item ID',
            'item_name' => 'Item Name',
            'item_desc' => 'Item Desc',
            'item_brand_id' => 'Item Brand ID',
            'primary_img' => 'Primary Img',
            'primary_category_id' => 'Primary Category ID',
            'sub_category_id' => 'Sub Category ID',
            'maximum_retail_price' => 'Maximum Retail Price',
            'is_tax_inclusive' => 'Is Tax Inclusive',
            'item_tax_code' => 'Item Tax Code',
            'is_customizable' => 'Is Customizable',
            'is_veg' => 'Is Veg',
            'is_refill_item' => 'Is Refill Item',
            'item_type' => 'Item Type',
            'item_weight' => 'Item Weight',
            'img_url_1' => 'Img Url 1',
            'img_url_2' => 'Img Url 2',
            'img_url_3' => 'Img Url 3',
            'created_date' => 'Created Date',
            'last_modified_date' => 'Last Modified Date',
            'last_updated_by' => 'Last Updated By',
        ];
    }

    public function getPrimaryCategory() {
        return $this->hasOne(ItemCategories::className(), ['category_id' => 'primary_category_id']);
    }

    public function getSubCategory() {
        return $this->hasOne(ItemSubCategories::className(), ['sub_category_id' => 'sub_category_id']);
    }

    public function getBrand(){
        return $this->hasOne(ItemBrandsModel::className(), ['brand_id' => 'item_brand_id']);
    }

}
