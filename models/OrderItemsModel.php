<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_items".
 *
 * @property int $order_item_id
 * @property int $order_id
 * @property int $purchase_id
 * @property int $restaurant_id
 * @property int|null $customer_id
 * @property int $item_id
 * @property int|null $item_store_mapping_id
 * @property string $item_name
 * @property string|null $item_img
 * @property int $quantity
 * @property string|null $order_item_attribute
 * @property string|null $instruction
 * @property float $item_unit_price
 * @property float|null $item_unit_parcel_charge
 * @property float|null $item_unit_parcel_tax
 * @property string|null $is_parcel_tax_applied
 * @property string|null $is_tax_inclusive
 * @property float|null $unit_tax_amount
 * @property string|null $item_tax_code
 * @property float|null $item_unit_weight
 * @property float|null $item_tax_rate
 * @property float|null $order_item_tax
 * @property float|null $order_item_amount
 * @property float|null $order_item_weight
 * @property string $order_item_category
 * @property string|null $kot_printer_name
 * @property string $item_status
 * @property string|null $is_veg
 * @property string $is_parcel
 * @property int $create_date
 * @property int $last_modified_date
 */
class OrderItemsModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_items';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('purchaseDB');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'purchase_id', 'restaurant_id', 'item_id', 'item_name', 'quantity', 'item_unit_price', 'item_status', 'create_date', 'last_modified_date'], 'required'],
            [['order_id', 'purchase_id', 'restaurant_id', 'customer_id', 'item_id', 'item_store_mapping_id', 'quantity', 'create_date', 'last_modified_date'], 'integer'],
            [['order_item_attribute'], 'safe'],
            [['item_unit_price', 'item_unit_parcel_charge', 'item_unit_parcel_tax', 'unit_tax_amount', 'item_unit_weight', 'item_tax_rate', 'order_item_tax', 'order_item_amount', 'order_item_weight'], 'number'],
            [['item_name', 'instruction'], 'string', 'max' => 500],
            [['item_img'], 'string', 'max' => 1000],
            [['is_parcel_tax_applied', 'is_tax_inclusive', 'is_veg', 'is_parcel'], 'string', 'max' => 2],
            [['item_tax_code', 'item_status'], 'string', 'max' => 50],
            [['order_item_category', 'kot_printer_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_item_id' => 'Order Item ID',
            'order_id' => 'Order ID',
            'purchase_id' => 'Purchase ID',
            'restaurant_id' => 'Restaurant ID',
            'customer_id' => 'Customer ID',
            'item_id' => 'Item ID',
            'item_store_mapping_id' => 'Item Store Mapping ID',
            'item_name' => 'Item Name',
            'item_img' => 'Item Img',
            'quantity' => 'Quantity',
            'order_item_attribute' => 'Order Item Attribute',
            'instruction' => 'Instruction',
            'item_unit_price' => 'Item Price',
            'item_unit_parcel_charge' => 'Item Unit Parcel Charge',
            'item_unit_parcel_tax' => 'Item Unit Parcel Tax',
            'is_parcel_tax_applied' => 'Is Parcel Tax Applied',
            'is_tax_inclusive' => 'Is Tax Inclusive',
            'unit_tax_amount' => 'Unit Tax Amount',
            'item_tax_code' => 'Item Tax Code',
            'item_unit_weight' => 'Item Unit Weight',
            'item_tax_rate' => 'Item Tax Rate',
            'order_item_tax' => 'Order Item Tax',
            'order_item_amount' => 'Total Item Amount',
            'order_item_weight' => 'Order Item Weight',
            'order_item_category' => 'Order Item Category',
            'kot_printer_name' => 'Kot Printer Name',
            'item_status' => 'Item Status',
            'is_veg' => 'Is Veg',
            'is_parcel' => 'Is Parcel',
            'create_date' => 'Create Date',
            'last_modified_date' => 'Last Modified Date',
        ];
    }
}
