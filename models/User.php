<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "person".
 *
 * @property int $person_id
 * @property string $first_name
 * @property string|null $last_name
 * @property string|null $email_id
 * @property string|null $username
 * @property string|null $password
 * @property int|null $phone_number
 * @property int $role_id
 * @property string|null $designation
 * @property int $restaurant_group_id
 * @property int $restaurant_id
 * @property string $verified
 * @property int $created_at
 * @property int $updated_at
 * @property int $last_updated_by
 */
class User extends ActiveRecord implements IdentityInterface {

    const STATUS_DELETED = "DELETED";
    const STATUS_ACTIVE = "ACTIVE";
    

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'person';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['first_name', 'role_id', 'restaurant_group_id', 'restaurant_id', 'created_at', 'updated_at', 'last_updated_by'], 'required'],
            [['phone_number', 'role_id', 'restaurant_group_id', 'restaurant_id', 'created_at', 'updated_at', 'last_updated_by'], 'integer'],
            [['first_name', 'last_name'], 'string', 'max' => 50],
            [['email_id', 'username', 'designation', 'record_status'], 'string', 'max' => 100],
            [['password'], 'string', 'max' => 255],
            [['verified'], 'string', 'max' => 2],
            [['username'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'person_id' => 'Person Id',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email_id' => 'Email Id',
            'username' => 'Username',
            'password' => 'Password',
            'phone_number' => 'Phone Number',
            'role_id' => 'Role Id',
            'designation' => 'Designation',
            'restaurant_group_id' => 'Restaurant Group Id',
            'restaurant_id' => 'Restaurant Id',
            'verified' => 'Verified',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'record_status' => 'Status',
            'last_updated_by' => 'Last Updated By',
        ];
    }

    public static function getFullName() {
        return Yii::$app->user->identity->first_name . ' ' . Yii::$app->user->identity->last_name;
    }

    /*
     * *********************************************************************
     * ***                                                               ***
     * ***    SECTION:Implementation of IdentityInterface Methods        ***
     * ***                                                               ***
     * *********************************************************************
     */

    /**
     * 
     * Finds an identity by the given ID.
     * Null is returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     *
     * @param int $id the ID to be looked for
     * 
     * @return User(identityObj that matches given ID)|null
     */
    public static function findIdentity($id) {
        return static::findOne(['person_id' => $id, 'record_status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by username
     * 
     * Null is returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     *
     * @param string $username the user ID to be looked for
     * 
     * @return User(identityObj that matches given username)|null
     */
    public static function findByUsername($username) {
        return static::findOne(['username' => $username, 'record_status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds an identity by the given token.
     * 
     * Null is returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     * 
     * @param string $token the token to be looked for
     * @param mixed $type the type of the token.
     * 
     * @return User(identityObj that matches given username)|null
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * @return string a key that is used to check the validity of a given identity ID.
     */
    public function getAuthKey(): string {
        return $this->access_token;
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * 
     * @return int an ID that uniquely identifies a user identity.
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    /**
     * Validates the given auth key.
     * 
     * This is required if [[User::enableAutoLogin]] is enabled.
     * 
     * @param string $authKey the given auth key
     * 
     * @return bool whether the given auth key is valid.
     */
    public function validateAuthKey($authKey): bool {
        return $this->access_token === $authKey;
    }

    /**
     * Generates Authentication Key to be stored against User
     * 
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * TODO : Invalidate earlier issued authKeys when you implement force user logout, password change and
     * other scenarios, that require forceful access revocation for old sessions.
     *
     */
    public function generateAuthKey() {
        $this->access_token = Yii::$app->security->generateRandomString();
    }

    public function removeAuthKey() {
        $this->access_token = '';
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password) {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password) {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

}
