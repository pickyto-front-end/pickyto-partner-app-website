<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CustomerOrdersModel;

/**
 * CustomerOrdersSearch represents the model behind the search form of `app\models\CustomerOrdersModel`.
 */
class CustomerOrdersSearch extends CustomerOrdersModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'purchase_id', 'restaurant_id', 'customer_id', 'pickup_point_id', 'delivery_address_id', 'delivery_pin', 'delivery_assoc_id', 'phone_no', 'create_date', 'last_modified_date', 'last_updated_by'], 'integer'],
            [['is_to_go_parcel', 'is_delivery', 'order_status', 'order_source', 'order_category', 'order_notes', 'is_delivery_pin_verified', 'delivery_status', 'delivery_time_estimate', 'customer_name', 'order_type', 'is_scheduled', 'schedule_details'], 'safe'],
            [['order_amount', 'discounted_order_amount', 'tax_amount', 'parcel_charge', 'parcel_tax_amt', 'delivery_charge', 'convenience_fee', 'delivery_distance'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomerOrdersModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order_id' => $this->order_id,
            'purchase_id' => $this->purchase_id,
            'restaurant_id' => $this->restaurant_id,
            'customer_id' => $this->customer_id,
            'pickup_point_id' => $this->pickup_point_id,
            'delivery_address_id' => $this->delivery_address_id,
            'order_amount' => $this->order_amount,
            'discounted_order_amount' => $this->discounted_order_amount,
            'tax_amount' => $this->tax_amount,
            'parcel_charge' => $this->parcel_charge,
            'parcel_tax_amt' => $this->parcel_tax_amt,
            'delivery_charge' => $this->delivery_charge,
            'convenience_fee' => $this->convenience_fee,
            'delivery_pin' => $this->delivery_pin,
            'delivery_assoc_id' => $this->delivery_assoc_id,
            'delivery_distance' => $this->delivery_distance,
            'phone_no' => $this->phone_no,
            'create_date' => $this->create_date,
            'last_modified_date' => $this->last_modified_date,
            'last_updated_by' => $this->last_updated_by,
        ]);

        $query->andFilterWhere(['like', 'is_to_go_parcel', $this->is_to_go_parcel])
            ->andFilterWhere(['like', 'is_delivery', $this->is_delivery])
            ->andFilterWhere(['like', 'order_status', $this->order_status])
            ->andFilterWhere(['like', 'order_source', $this->order_source])
            ->andFilterWhere(['like', 'order_category', $this->order_category])
            ->andFilterWhere(['like', 'order_notes', $this->order_notes])
            ->andFilterWhere(['like', 'is_delivery_pin_verified', $this->is_delivery_pin_verified])
            ->andFilterWhere(['like', 'delivery_status', $this->delivery_status])
            ->andFilterWhere(['like', 'delivery_time_estimate', $this->delivery_time_estimate])
            ->andFilterWhere(['like', 'customer_name', $this->customer_name])
            ->andFilterWhere(['like', 'order_type', $this->order_type])
            ->andFilterWhere(['like', 'is_scheduled', $this->is_scheduled])
            ->andFilterWhere(['like', 'schedule_details', $this->schedule_details]);

        return $dataProvider;
    }
}
