<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer_orders".
 *
 * @property int $order_id
 * @property int $purchase_id
 * @property int $restaurant_id
 * @property int|null $customer_id
 * @property int|null $pickup_point_id
 * @property int|null $delivery_address_id
 * @property string $is_to_go_parcel
 * @property string $is_delivery
 * @property float|null $order_amount
 * @property float|null $discounted_order_amount
 * @property float|null $tax_amount
 * @property float|null $parcel_charge
 * @property float|null $parcel_tax_amt
 * @property float|null $delivery_charge
 * @property float|null $convenience_fee
 * @property string|null $order_status
 * @property string|null $order_source
 * @property string $order_category
 * @property string|null $order_notes
 * @property int|null $delivery_pin
 * @property string $is_delivery_pin_verified
 * @property string|null $delivery_status
 * @property int|null $delivery_assoc_id
 * @property float|null $delivery_distance
 * @property string|null $delivery_time_estimate
 * @property string|null $customer_name
 * @property int|null $phone_no
 * @property string|null $order_type
 * @property string|null $is_scheduled
 * @property string|null $schedule_details
 * @property int $create_date
 * @property int $last_modified_date
 * @property int|null $last_updated_by
 */
class CustomerOrdersModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer_orders';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('purchaseDB');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['purchase_id', 'restaurant_id', 'create_date', 'last_modified_date'], 'required'],
            [['purchase_id', 'restaurant_id', 'customer_id', 'pickup_point_id', 'delivery_address_id', 'delivery_pin', 'delivery_assoc_id', 'phone_no', 'create_date', 'last_modified_date', 'last_updated_by'], 'integer'],
            [['order_amount', 'discounted_order_amount', 'tax_amount', 'parcel_charge', 'parcel_tax_amt', 'delivery_charge', 'convenience_fee', 'delivery_distance'], 'number'],
            [['delivery_time_estimate', 'schedule_details'], 'safe'],
            [['is_to_go_parcel', 'is_delivery', 'is_delivery_pin_verified'], 'string', 'max' => 2],
            [['order_status', 'order_source', 'order_category', 'delivery_status'], 'string', 'max' => 100],
            [['order_notes'], 'string', 'max' => 500],
            [['customer_name', 'order_type'], 'string', 'max' => 45],
            [['is_scheduled'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'purchase_id' => 'Purchase ID',
            'restaurant_id' => 'Restaurant ID',
            'customer_id' => 'Customer ID',
            'pickup_point_id' => 'Pickup Point ID',
            'delivery_address_id' => 'Delivery Address ID',
            'is_to_go_parcel' => 'Is To Go Parcel',
            'is_delivery' => 'Is Delivery',
            'order_amount' => 'Total Order Amount',
            'discounted_order_amount' => 'Order Amount',
            'tax_amount' => 'Tax Amount',
            'parcel_charge' => 'Parcel Charge',
            'parcel_tax_amt' => 'Parcel Tax Amt',
            'delivery_charge' => 'Delivery Charge',
            'convenience_fee' => 'Convenience Fee',
            'order_status' => 'Order Status',
            'order_source' => 'Order Source',
            'order_category' => 'Order Category',
            'order_notes' => 'Order Notes',
            'delivery_pin' => 'Delivery Pin',
            'is_delivery_pin_verified' => 'Is Delivery Pin Verified',
            'delivery_status' => 'Delivery Status',
            'delivery_assoc_id' => 'Delivery Assoc ID',
            'delivery_distance' => 'Delivery Distance',
            'delivery_time_estimate' => 'Delivery Time Estimate',
            'customer_name' => 'Customer Name',
            'phone_no' => 'Phone No',
            'order_type' => 'Order Type',
            'is_scheduled' => 'Is Scheduled',
            'schedule_details' => 'Schedule Details',
            'create_date' => 'Ordered Time',
            'last_modified_date' => 'Last Modified Date',
            'last_updated_by' => 'Last Updated By',
        ];
    } //end func


    /**
     * Generic method in model to update the order_status column by orderId
     */
    public static function updateOrderStatusByOrderId($orderId, $orderStatus) {

       $response =  Yii::$app->purchaseDB
                    ->createcommand()
                    ->update(
                            CustomerOrdersModel::tableName(), 
                            [
                                'order_status' => $orderStatus,
                            ],
                            'order_id = '.$orderId
                        )->execute();

        return $response;

    }//end func
}
