<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "restaurant_details".
 *
 * @property int $restaurant_detail_entry_id
 * @property int $restaurant_id
 * @property int $group_id
 * @property string $restaurant_name
 * @property string $restaurant_type
 * @property string|null $is_product_code_used
 * @property string|null $access_control_type
 * @property string|null $app_signup
 * @property string|null $website_signup
 * @property string|null $kiosk_signup
 * @property string|null $digital_menu_checkout
 * @property string|null $menu_QR_code_availability
 * @property string|null $multi_course_signup
 * @property string|null $convenience_fee_availability
 * @property int|null $commission_fee
 * @property string|null $delivery_service_provider
 * @property string|null $payment_strategy
 * @property int|null $created_date
 * @property int|null $last_modified_date
 * @property int|null $last_modified_by
 */
class RestaurantDetailsModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'restaurant_details';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_restaurant');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['restaurant_id', 'group_id', 'restaurant_name', 'restaurant_type'], 'required'],
            [['restaurant_id', 'group_id', 'commission_fee', 'created_date', 'last_modified_date', 'last_modified_by'], 'integer'],
            [['restaurant_name', 'restaurant_type', 'access_control_type', 'delivery_service_provider', 'payment_strategy'], 'string', 'max' => 100],
            [['is_product_code_used', 'app_signup', 'website_signup', 'kiosk_signup', 'digital_menu_checkout', 'menu_QR_code_availability', 'multi_course_signup', 'convenience_fee_availability'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'restaurant_detail_entry_id' => 'Restaurant Detail Entry ID',
            'restaurant_id' => 'Restaurant ID',
            'group_id' => 'Group ID',
            'restaurant_name' => 'Restaurant Name',
            'restaurant_type' => 'Restaurant Type',
            'is_product_code_used' => 'Is Product Code Used',
            'access_control_type' => 'Access Control Type',
            'app_signup' => 'App Signup',
            'website_signup' => 'Website Signup',
            'kiosk_signup' => 'Kiosk Signup',
            'digital_menu_checkout' => 'Digital Menu Checkout',
            'menu_QR_code_availability' => 'Menu Qr Code Availability',
            'multi_course_signup' => 'Multi Course Signup',
            'convenience_fee_availability' => 'Convenience Fee Availability',
            'commission_fee' => 'Commission Fee',
            'delivery_service_provider' => 'Delivery Service Provider',
            'payment_strategy' => 'Payment Strategy',
            'created_date' => 'Created Date',
            'last_modified_date' => 'Last Modified Date',
            'last_modified_by' => 'Last Modified By',
        ];
    }
}
