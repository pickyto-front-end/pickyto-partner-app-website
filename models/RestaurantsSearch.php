<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RestaurantsModel;

/**
 * RestaurantsSearch represents the model behind the search form of `app\models\RestaurantsModel`.
 */
class RestaurantsSearch extends RestaurantsModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['restaurant_id', 'group_id', 'max_capacity', 'opening_time', 'closing_time', 'delivery_service_start_time', 'delivery_service_end_time', 'commission_pct', 'primary_phone', 'secondary_phone', 'bank_account_no', 'gen_prep_time', 'cost_per_person', 'created_date', 'last_modified_date', 'last_updated_by'], 'integer'],
            [['restaurant_name', 'fssai_number', 'restaurant_desc', 'restaurant_address_id', 'restaurant_timings', 'operating_days', 'is_open_today', 'is_parcel_available', 'is_delivery_service_available', 'is_convenience_fee_charged', 'parcel_service_availability', 'parking_availability', 'wheelchair_accessible', 'logo_img_url', 'banner_img_url', 'thumbnail_img_url', 'cuisine_type', 'store_type', 'restaurant_status', 'company_legal_id', 'company_tax_id', 'address', 'locality', 'city', 'state', 'country', 'onboard_status', 'branch_name', 'primary_email', 'secondary_email', 'currency_code', 'bank_name', 'bank_account_name', 'IFSC', 'bank_branch', 'restaurant_referral_code', 'external_account_id', 'legal_name', 'trade_name'], 'safe'],
            [['latitude', 'longitude', 'delivery_range'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RestaurantsModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'restaurant_id' => $this->restaurant_id,
            'group_id' => $this->group_id,
            'max_capacity' => $this->max_capacity,
            'opening_time' => $this->opening_time,
            'closing_time' => $this->closing_time,
            'delivery_service_start_time' => $this->delivery_service_start_time,
            'delivery_service_end_time' => $this->delivery_service_end_time,
            'commission_pct' => $this->commission_pct,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'delivery_range' => $this->delivery_range,
            'primary_phone' => $this->primary_phone,
            'secondary_phone' => $this->secondary_phone,
            'bank_account_no' => $this->bank_account_no,
            'gen_prep_time' => $this->gen_prep_time,
            'cost_per_person' => $this->cost_per_person,
            'created_date' => $this->created_date,
            'last_modified_date' => $this->last_modified_date,
            'last_updated_by' => $this->last_updated_by,
        ]);

        $query->andFilterWhere(['like', 'restaurant_name', $this->restaurant_name])
            ->andFilterWhere(['like', 'fssai_number', $this->fssai_number])
            ->andFilterWhere(['like', 'restaurant_desc', $this->restaurant_desc])
            ->andFilterWhere(['like', 'restaurant_address_id', $this->restaurant_address_id])
            ->andFilterWhere(['like', 'restaurant_timings', $this->restaurant_timings])
            ->andFilterWhere(['like', 'operating_days', $this->operating_days])
            ->andFilterWhere(['like', 'is_open_today', $this->is_open_today])
            ->andFilterWhere(['like', 'is_parcel_available', $this->is_parcel_available])
            ->andFilterWhere(['like', 'is_delivery_service_available', $this->is_delivery_service_available])
            ->andFilterWhere(['like', 'is_convenience_fee_charged', $this->is_convenience_fee_charged])
            ->andFilterWhere(['like', 'parcel_service_availability', $this->parcel_service_availability])
            ->andFilterWhere(['like', 'parking_availability', $this->parking_availability])
            ->andFilterWhere(['like', 'wheelchair_accessible', $this->wheelchair_accessible])
            ->andFilterWhere(['like', 'logo_img_url', $this->logo_img_url])
            ->andFilterWhere(['like', 'banner_img_url', $this->banner_img_url])
            ->andFilterWhere(['like', 'thumbnail_img_url', $this->thumbnail_img_url])
            ->andFilterWhere(['like', 'cuisine_type', $this->cuisine_type])
            ->andFilterWhere(['like', 'store_type', $this->store_type])
            ->andFilterWhere(['like', 'restaurant_status', $this->restaurant_status])
            ->andFilterWhere(['like', 'company_legal_id', $this->company_legal_id])
            ->andFilterWhere(['like', 'company_tax_id', $this->company_tax_id])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'locality', $this->locality])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'onboard_status', $this->onboard_status])
            ->andFilterWhere(['like', 'branch_name', $this->branch_name])
            ->andFilterWhere(['like', 'primary_email', $this->primary_email])
            ->andFilterWhere(['like', 'secondary_email', $this->secondary_email])
            ->andFilterWhere(['like', 'currency_code', $this->currency_code])
            ->andFilterWhere(['like', 'bank_name', $this->bank_name])
            ->andFilterWhere(['like', 'bank_account_name', $this->bank_account_name])
            ->andFilterWhere(['like', 'IFSC', $this->IFSC])
            ->andFilterWhere(['like', 'bank_branch', $this->bank_branch])
            ->andFilterWhere(['like', 'restaurant_referral_code', $this->restaurant_referral_code])
            ->andFilterWhere(['like', 'external_account_id', $this->external_account_id])
            ->andFilterWhere(['like', 'legal_name', $this->legal_name])
            ->andFilterWhere(['like', 'trade_name', $this->trade_name]);

        return $dataProvider;
    }
}
