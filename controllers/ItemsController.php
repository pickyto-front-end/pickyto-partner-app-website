<?php

namespace app\controllers;

use app\components\utils\LassiDAOUtils;
use app\components\utils\UserDetails;
use app\models\ItemCategoriesModel;
use Yii;
use app\models\ItemDetailsModel;
use app\models\ItemDetailsSearch;
use app\models\ItemStoreMappingModel;
use app\models\ItemSubCategoriesModel;
use app\models\MasterItemDetails;
use app\models\MasterItemDetailsModel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ItemsController implements the CRUD actions for ItemDetailsModel model.
 */
class ItemsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ItemDetailsModel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $storeId = UserDetails::get_restaurant_id();

        $storeMappings = ItemStoreMappingModel::find(['primary_category_id'])->where(['store_id' => $storeId])->all();

        $primaryCategoryIds = array_column($storeMappings, "primary_category_id");

        $itemCategories = ItemCategoriesModel::find()->where(['category_id' => $primaryCategoryIds])->all();

        return $this->render('index', [
            'model' => $itemCategories,
        ]);

    }//end func


    /**
     * 
     * @return mixed
     */
    public function actionViewSubCats($categoryId)
    {

        $subCategories = ItemSubCategoriesModel::find()->where(['primary_category_id' => $categoryId])->all();
        
        return $this->render('sub-cats', [
            'subCats' => $subCategories,
            'categoryId' => $categoryId,
        ]);

    }//end func

    /**
     
     * @return mixed
     */
    public function actionViewItemsBySubCats($subCategoryId, $categoryId)
    {

        $storeMappings = ItemStoreMappingModel::find()->where(['sub_category_id' => $subCategoryId, 'store_id' => UserDetails::get_restaurant_id()])->all();

        $itemIds = array_column($storeMappings, 'item_id');
        
        $masterItems = MasterItemDetails::find()->where(['item_id' => $itemIds])->all();

        $indexedMasterItems = LassiDAOUtils::pivotARObjByIndex($masterItems, 'item_id');

        return $this->render('list-items', [
            'storeItems' => $storeMappings,
            'masterItems' => $indexedMasterItems,
            'subCategoryId' => $subCategoryId,
            'categoryId' => $categoryId,
        ]);

    }//end func




    /**
     * Displays a single ItemDetailsModel model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ItemDetailsModel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ItemDetailsModel();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->item_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ItemDetailsModel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($itemId, $itemStoreId)
    {
        $model = ItemStoreMappingModel::findOne($itemStoreId);


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //save also the values updated in Master Item Details
            $postParams = Yii::$app->request->post();

            //get the Master Items isVeg post param value
            $isVeg = $postParams['MasterItemDetails']['is_veg'];

            $itemName = $postParams['MasterItemDetails']['item_name'];

            $masterItem = MasterItemDetails::findOne($model->item_id);

            $masterItem->is_veg = $isVeg;

            $masterItem->item_name = $itemName;

            $masterItem->save(false);

            return $this->redirect(['view-items-by-sub-cats', 'categoryId'=> $model->primary_category_id, 'subCategoryId' => $model->sub_category_id]);
        }

        $masterItem = MasterItemDetails::findOne($itemId);

        return $this->render('update', [
            'model' => $model,
            'masterItem' => $masterItem,
        ]);
    }

    /**
     * Deletes an existing ItemDetailsModel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ItemDetailsModel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ItemDetailsModel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ItemStoreMappingModel::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
