<?php

namespace app\controllers;

use Yii;

use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\components\utils\UserDetails;
use  app\components\types\OrderStatusEnum;
use app\models\CustomerOrdersModel;
use app\models\OrderItemsModel;

/**
 * OrdersController implements the CRUD actions for CustomerOrdersModel model.
 */
class OrdersController extends Controller
{
    

    /**
     * Lists all CustomerOrdersModel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $storeId = UserDetails::get_restaurant_id();

        $paymentCompletedOrders = CustomerOrdersModel::find()
                            ->where([
                                'order_status' => OrderStatusEnum::PAYMENT_COMPLETED, 
                                'restaurant_id' => $storeId,
                                ])
                            ->limit(20)
                            ->orderBy(['create_date' => SORT_ASC])
                            ->all();

        return $this->render('index', [
            'paymentCompletedOrders' => $paymentCompletedOrders,
        ]);
    }//end func


    public function actionAcceptedOrders(){
        $storeId = UserDetails::get_restaurant_id();

        $acceptedOrders = CustomerOrdersModel::find()
                        ->where([
                            'order_status' => OrderStatusEnum::RESTAURANT_CONFIRMED, 
                            'restaurant_id' => $storeId,
                            ])
                        ->limit(20)
                        ->orderBy(['create_date' => SORT_ASC])
                        ->all();
        
        return $this->render('accepted-orders', [
            'acceptedOrders' => $acceptedOrders,
        ]);


    }//end func


    public function actionReadyOrders () {
        $storeId = UserDetails::get_restaurant_id();

        $readyOrders = CustomerOrdersModel::find()
                    ->where([
                        'order_status' => OrderStatusEnum::PENDING_PICKUP, 
                        'restaurant_id' => $storeId,
                        ])
                    ->limit(20)
                    ->orderBy(['create_date' => SORT_ASC])
                    ->all();


        return $this->render('ready-orders', [
            'readyOrders' => $readyOrders,
        ]);


    }//end func

    /**
     * Displays a single CustomerOrdersModel model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {


        $orderItems = new ActiveDataProvider ([
            'query' => OrderItemsModel::find()
                            ->where(['order_id' => $id]),
                'pagination' => [
                    'pageSize' => 30,
                ],
                'sort' => [
                            'defaultOrder' =>  [
                                'last_modified_date' => SORT_DESC,
                            ],
                        ],
            ]);


        return $this->render('view', [
            'model' => $this->findModel($id),
            'orderItems' => $orderItems,
        ]);
    } //end func

    /**
     * Finds the CustomerOrdersModel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomerOrdersModel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CustomerOrdersModel::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    /**
     * 
     */
    public function actionAcceptOrder($orderId) {

        if (empty($orderId)) {
            //bad request. nothing to process. redirect to orders page
            Yii::$app->session->setFlash('error', "There was an Error. No Order Id passed. Please try again");
            return $this->redirect(['index']);
        }

        //update order status to "restaurant confirmed"
        $response = CustomerOrdersModel::updateOrderStatusByOrderId($orderId, OrderStatusEnum::RESTAURANT_CONFIRMED);

        if (!$response){
            Yii::$app->session->setFlash('error', "Error Updating Status. Please try again ");
            return $this->redirect(['view',
                'id' => $orderId,
            ]);
        }

        Yii::$app->session->setFlash('success', "Order: $orderId - accepted successfully ");
        return $this->redirect(['index']);
    
    
    }//end func

    /**
     * 
     */
    public function actionMarkReady($orderId) {

        if (empty($orderId)) {
            //bad request. nothing to process. redirect to orders page
            Yii::$app->session->setFlash('error', "There was an Error. No Order Id passed. Please try again");
            return $this->redirect(['index']);
        }

        //update order status to "restaurant confirmed"
        $response = CustomerOrdersModel::updateOrderStatusByOrderId($orderId, OrderStatusEnum::PENDING_PICKUP);

        if (!$response){
            Yii::$app->session->setFlash('error', "Error Updating Status. Please try again ");
            return $this->redirect(['view',
                'id' => $orderId,
            ]);
        }

        Yii::$app->session->setFlash('success', "Order: $orderId - accepted successfully ");
        return $this->redirect(['accepted-orders']);
    
    
    }//end func

     /**
     * 
     */
    public function actionMarkDelivered($orderId) {

        if (empty($orderId)) {
            //bad request. nothing to process. redirect to orders page
            Yii::$app->session->setFlash('error', "There was an Error. No Order Id passed. Please try again");
            return $this->redirect(['index']);
        }

        //update order status to "restaurant confirmed"
        $response = CustomerOrdersModel::updateOrderStatusByOrderId($orderId, OrderStatusEnum::ORDER_DELIVERED);

        if (!$response){
            Yii::$app->session->setFlash('error', "Error Updating Status. Please try again ");
            return $this->redirect(['view',
                'id' => $orderId,
            ]);
        }

        Yii::$app->session->setFlash('success', "Order: $orderId - accepted successfully ");
        return $this->redirect(['ready-orders']);
    
    
    }//end func

     /**
     * 
     */
    public function actionRejectOrder($orderId) {

        if (empty($orderId)) {
            //bad request. nothing to process. redirect to orders page
            Yii::$app->session->setFlash('error', "There was an Error. No Order Id passed. Please try again");
            return $this->redirect(['index']);
        }

        //update order status to "restaurant confirmed"
        $response = CustomerOrdersModel::updateOrderStatusByOrderId($orderId, OrderStatusEnum::ORDER_DECLINED_BY_RESTAURANT);

        if (!$response){
            Yii::$app->session->setFlash('error', "Error Updating Status. Please try again ");
            return $this->redirect(['view',
                'id' => $orderId,
            ]);
        }

        Yii::$app->session->setFlash('success', "Order: $orderId - Declined / Cancelled Ssuccessfully ");
        return $this->redirect(['index']);
    
    
    }//end func

    

}
