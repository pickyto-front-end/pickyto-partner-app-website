<?php

namespace app\components\types;

/**
 * Description of PurchaseStatusEnum
 * 
 * This Enum encapsulates the different status that an Purchase can have 
 * Simple utility class to model enum similar to Java 
 * 
 * Purchase Status captures wholistic status of the sub-orders in the purchase. 
 * Hence primarily deals with Payment Status and Open/Close status of the entire purchase
 *
 * @author bharathi@sigaramtech.com
 */
class PurchaseStatusEnum {
    
    //Purchase is Open and is in its initial stages. 
    const OPEN_STATUS = "OPEN";
    
    /**
     * Status represents pending payment collection from the customer. 
     */
    const PENDING_PAYMENT = "PENDING_PAYMENT";
    
    /**
     * Payment Completed status.
     * Used by all clients
     */
    const PAYMENT_COMPLETED = "PAYMENT_COMPLETED";    
    
    /**
     * The entire purchase is closed successfully.
     * All Payment and Food Delivery is completed successfully.
     */
    const CLOSED_SUCCESS = "CLOSED_SUCCESS";
    
    /**
     * Purchase is closed due to some other reason. 
     * More details are provided in description field
     * OR See details in Order Status columns
     */
    const CLOSED_OTHER = "CLOSED_OTHER";
    
    /**
     * Payment was NOT completed successfully 
     * OR 
     * Food Delivered was not satisfactory. So payment got declined.
     * In either case this purchase is being marked as Closed UNSuccessful
     */
    const CLOSED_DECLINED = "CLOSED_UN_SUCCESSFUL";
    
    /**
     * Order is being canceled due to payment decline.
     * This can technically not happen in India,but can happen in other marketplaces
     */
    const PURCHASE_CANCELED_PAYMENT_DECLINE = "PAYMENT_DECLINE_CANCEL";

    

}
