<?php

namespace app\components\types;

/**
 * Description of OrderStatusEnum
 * 
 * This Enum encapsulates the different status that an Order can have 
 * Simple utility class to model enum similar to Java 
 * 
 * None of Payment Issues are being tracked in Order Status
 *
 * @author bharathi@sigaramtech.com
 */
class OrderStatusEnum {

    /**
     * The Parent Purchase is Open and items being ordered is in the Cart
     * Status => CART
     */
    const CART_STATUS = "CART";
    
    
    /**
     * Payment Completed status.
     * Used by Parent Purchase As well.
     * Next logical state after Cart.
     */
    const PAYMENT_COMPLETED = "PAYMENT_COMPLETED";
        
    /**
     * Customer Order has been ACCEPTED / CONFIRMED by restaurant 
     * and the order is going to PREPARED by the restaurant. 
     * TYPICALLY KOT IS PRINTED after being confirmed by Restaurant
     */
    const RESTAURANT_CONFIRMED = "ORDER_CONFIRMED_BY_REST";

    /**
     * Restaurant declined the order before confirming the order.
     * This can happen if the item is out of stock and etc. 
     */
    const ORDER_DECLINED_BY_RESTAURANT = "ORDER_DECLINED_BY_RESTAURANT";

    /**
     * for some reason, even after confirming the order, the restaurant decides to
     * cancel the order due to issues like unavailability of food etc. 
     * This enum represents this state
     */
    const CANCELLED_BY_RESTAURANT = "ORDER_CANCELLED_BY_REST";

    /**
     * Restaurant updates that the order is COOKING status
     * Can be used by all clients 
     */
    const COOKING = "COOKING";

    /**
     * The order is now Cooked and ready to be picked up by the customer
     */
    const PENDING_PICKUP = "PENDING_PICKUP_BY_CUST";
    
    
    /**
     * The customer never picked up the order
     */
    const NEVER_PICKED_UP = "NEVER_PICKED_BY_CUST";

    /**
     * Customer order was delivered or fulfilled successfully to the customer
     * used by all clients
     */
    const ORDER_DELIVERED = "ORDER_DELIVERED_SUCCESS";

    /**
     * Some of the items on the order had issues during delivery. hence partial success
     * But not canceled.
     */
    const ORDER_DELIVERED_W_PROB = "ORDER_DELIVERED_W_PROBLEMS";

    /**
     * The item delivered to the customer was the wrong item or 
     * there was some mismatch in the items being order. 
     * The order was canceled due to wrong item being ordered
     */
    const ORDER_CANCELED_WRONG_ITEM = "ORDER_CANCEL_WRONG_ITEM";

    /**
     * Order is canceled for other reasons. See purchase details for more info.
     */
    const ORDER_CANCELED_OTHER = "ORDER_CANCEL_OTHER";

    /**
     * Terminal state and is changed after the parent Purchase status 
     * is also set a CLOSED_SUCCESS
     */
    const CLOSED_SUCCESS = "CLOSED_SUCCESS";

    /**
     * Terminal State. The parent purchase and hence this order is closed   
     * due to some other reason. 
     * 
     */
    const CLOSED_OTHER = "CLOSED_OTHER";

    /**
     * Terminal State. Follows the Parent Purchase Obj Status.
     */
    const CLOSED_DECLINED = "CLOSED_UN_SUCCESSFUL";
    
    //order was cancelled by customer for some reason
    const CANCELLED_BY_CUSTOMER = "CANCELLED_BY_CUSTOMER";


    /*     * Grouping of Order Status Enums* */
    const CURRENT_STATUS_GROUP = "CURRENT";
    const PAST_STATUS_GROUP = "PAST";
    const COMPLETED_STATUS_GROUP = "COMPLETED";
    const CANCELLED_STATUS_GROUP = "CANCELLED";

    /*     * ************END GROUP ENUMS ********** */

    public static function getCurrentStatusEnums() {
        $list = array();

        //add/push all the status that are part of the OPEN or CURRENT Status for the customer
        array_push($list, static::PENDING_PICKUP);
        array_push($list, static::RESTAURANT_CONFIRMED);
        array_push($list, static::COOKING);
        array_push($list, PurchaseStatusEnum::PAYMENT_COMPLETED);

        return $list;
    }

//function

    public static function getPastOrderStatusEnums() {
        $list = array();

        array_push($list, static::CLOSED_DECLINED);
        array_push($list, static::CLOSED_OTHER);
        array_push($list, static::CLOSED_SUCCESS);
        array_push($list, static::ORDER_CANCELED_OTHER);
        array_push($list, static::ORDER_CANCELED_WRONG_ITEM);
        array_push($list, static::ORDER_DELIVERED);
        array_push($list, static::ORDER_DELIVERED_W_PROB);
        array_push($list, PurchaseStatusEnum::PURCHASE_CANCELED_PAYMENT_DECLINE);
        array_push($list, static::ORDER_DECLINED_BY_RESTAURANT);
        
        return $list;
    }

//function

    public static function getCompletedStatusEnums() {
        $list = array();

        array_push($list, static::CLOSED_DECLINED);
        array_push($list, static::CLOSED_OTHER);
        array_push($list, static::CLOSED_SUCCESS);
        array_push($list, static::ORDER_DELIVERED);
        array_push($list, static::ORDER_DELIVERED_W_PROB);
        array_push($list, static::NEVER_PICKED_UP);


        return $list;
    }//function

    public static function getCancelledStatusEnums() {
        $list = array();
        array_push($list, static::ORDER_CANCELED_OTHER);
        array_push($list, static::ORDER_CANCELED_WRONG_ITEM);
        array_push($list, static::ORDER_DECLINED_BY_RESTAURANT);
        array_push($list, static::CANCELLED_BY_RESTAURANT);
        array_push($list, static::CANCELLED_BY_CUSTOMER);

        return $list;
    }

//funct

    public static function getListForStatusGroup($grpStatus) {
        $orderStatusEnums = array();
        switch ($grpStatus) {
            case static::CURRENT_STATUS_GROUP:
                $orderStatusEnums = static::getCurrentStatusEnums();
                break;

            case static::PAST_STATUS_GROUP:
                $orderStatusEnums = static::getPastOrderStatusEnums();
                break;

            case static::COMPLETED_STATUS_GROUP:
                $orderStatusEnums = static::getCompletedStatusEnums();
                break;

            case static::CANCELLED_STATUS_GROUP:
                $orderStatusEnums = static::getCancelledStatusEnums();
                break;
        }//switch

        return $orderStatusEnums;
    }

//funct
}
