<?php

namespace app\components\types;


/**
 * Description of OrderItemStatusEnum
 * 
 * This Enum encapsulates the different status that an Order Item can have 
 * Simple utility class to model enum similar to Java
 *
 * @author bharathi@sigaramtech.com
 */
abstract class OrderItemStatusEnum {
    
    /**
     * The Parent Purchase & Order is Open and items being ordered is in the Cart
     * Status => CART
     */
    const CART_STATUS = "CART";
    
    /**
     * Customer Order has been ACCEPTED / CONFIRMED by restaurant 
     * and the order is going to PREPARED by the restaurant. 
     * TYPICALLY KOT IS PRINTED after being confirmed by Restaurant
     */
    const RESTAURANT_CONFIRMED = "CONFIRMED_BY_REST";
    
    /**
     * Restaurant updates that the order is COOKING status
     * Can be used by all clients 
     */
    const COOKING = "COOKING";
    
    /**
     * Used by the customer facing mobile app, where the customer needs to pick up
     * the item from restaurant or pickup point. 
     */
    const PENDING_PICKUP = "PENDING_PICKUP";
    
    /**
     * Item has been delivered
     */
    const ITEM_DELIVERED = "DELIVERED_SUCCESS";
    
    /**
     * The item delivered to the customer was the wrong item or 
     * there was some mismatch in the items being order. 
     * The Item was canceled due to wrong item being ordered
     */
    const CANCELED_WRONG_ITEM = "CANCEL_WRONG_ITEM";
    
    /**
     * There was some issue with the order, so this item is being given as FREE
     * complimentary
     */
    const FREE_ITEM = "FREE_ITEM";
    
}
