<?php

namespace app\components\utils;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * @author bharathi@
 * 
 * General DAO Utils class with a bunch of helper function to deal with 
 * 
 */
class LassiDAOUtils {

    /**
     * @TODO documentation 
     * 
     */
    public static function pivotArrayByObjKey($daoResult, $pivotKey) {
        if (empty($pivotKey)) {
            //log error / warn and return
            echo "ERROR: pivot key is null / empty ";
            return;
        }

        /**
         * Array Helper Util to the Rescue!! 
         * 
         * 
         * 
         */
        $responseData = ArrayHelper::index(ArrayHelper::toArray($daoResult, []), $pivotKey);

        // echo "<br> pivot key array helper: ";
        // print_r ($responseData);

        return $responseData;
    }


    //utility method to convert Active Record list to a MAP of Active Records by the given key.
    public static function pivotARObjByIndex ($ARList, $indexKey){

        $response = array();

        foreach ($ARList as $ARObj){
            $keyVal = $ARObj->$indexKey;

            $response[$keyVal] = $ARObj;

        }

        return $response;

    }//end func

//function

    /**
     * @TODO documentation 
     * 
     */
    public static function buildINQueryFromList($arrList, $key) {

        echo "<br> arr list :";
        print_r($arrList);

        for ($i = 0; $i <= count($arrList); $i++) {
            $responseData[$key] = $arrList[$i];
        }

//        echo "<br> IN Query from List => ";
//        print_r ($responseData);

        return $responseData;
    }

//function

    /**
     * 
     * @request     : DB Query Result Array & a pivot key to fetch the list
     * @response    : returns a array with the list of values of the pivot key from the input DB results array
     * 
     * 
     */
    public static function getValueListFromARList($inputArr, $pkey) {

        $responseData = array();

        foreach ($inputArr as $arrIndex => $ARResultObject) {

            array_push($responseData, $ARResultObject[$pkey]);
        }//
        // echo "value list from AR List: ";
        // print_r($responseData);

        return $responseData;
    }

//function

    /**
     * @TODO documentation 
     * 
     */
    public static function mergeItemDetailsNAttributesList($itemDetailsObj, $itemAttributesObj) {

        $itemDetailsArr = LassiDAOUtils::pivotArrayByObjKey($itemDetailsObj, "item_id");

        foreach ($itemAttributesObj as $index => $itemAttributes) {

            //fetch the itemId of the attribute
            $itemId = $itemAttributes['item_id'];

            //fetch the pertinent Item Details objects from the list of Item Details
            $itemDetails = $itemDetailsArr[$itemId];

            //fetch the attribute name and its value
            $attributeName = $itemAttributes['attribute_name'];

            $attributeValue = $itemAttributes['attribute_value'];

            //push the attribute name => attribute value mapping to the Item Details object
            $itemDetails[$attributeName] = $attributeValue;

            //push this current item Details obj into the list of item details obj array.
            $itemDetailsArr[$itemId] = $itemDetails;
        }//foreach

        return $itemDetailsArr;
    }

//function

    /**
     * @TODO: documentation
     */
    public static function mergeItemAttributesNItemDetailsSingle($itemDetailsObj, $itemAttributesObj) {

        $i = 1;
        foreach ($itemAttributesObj as $index => $itemAttributes) {

            $itemId = $itemAttributes['item_id'];


            $attributeName = $itemAttributes['attribute_name'];

            $attributeValue = $itemAttributes['attribute_value'];

            //there can be more than tag value with the same key, so add them into
            //the map with counter index
            if (!empty($itemDetailsObj[$attributeName])) {
                $attributeName .= "_$i";
                $i++;
            }

            $itemDetailsObj[$attributeName] = $attributeValue;
        }//foreach


        return $itemDetailsObj;
    }

//function

    /**
     * @input: An Array objects, which is essentially a Query Result, containing a set of data elements
     * 
     * @output: return a map with key as the index attribute or column of the table being queried. 
     *          The value if the list of table rows that contain that specific attribute value
     * 
     * Example Input Array Object: 
     *  
     * Array ( 
     *      [0] => Array ( [dining_table_id] => 1 [dining_table_name] => SOFA [restaurant_id] => 1 [dining_table_desc] => 2 can sit [max_capacity] => 2 [dinning_table_type] => 2 [dining_table_status] => ACTIVE [logo_img_url] => https://xyz.png [banner_img_url] => [thumbnail_img_url] => [created_date] => 23132 [last_modified_date] => 2311 [last_updated_by] => 2134242 ) 
     *      [1] => Array ( [dining_table_id] => 2 [dining_table_name] => SOFA [restaurant_id] => 1 [dining_table_desc] => 2 can sit [max_capacity] => 2 [dinning_table_type] => 2 [dining_table_status] => [logo_img_url] => https://xyz.png [banner_img_url] => [thumbnail_img_url] => [created_date] => 23132 [last_modified_date] => 2311 [last_updated_by] => 2134242 ) 
     *      [2] => Array ( [dining_table_id] => 3 [dining_table_name] => SOFA [restaurant_id] => 1 [dining_table_desc] => 2 can sit [max_capacity] => 2 [dinning_table_type] => 0 [dining_table_status] => [logo_img_url] => https://xyz.png [banner_img_url] => [thumbnail_img_url] => [created_date] => 23132 [last_modified_date] => 2311 [last_updated_by] => 2134242 ) 
     *      [3] => Array ( [dining_table_id] => 4 [dining_table_name] => SOFA [restaurant_id] => 1 [dining_table_desc] => 2 can sit [max_capacity] => 2 [dinning_table_type] => 0 [dining_table_status] => [logo_img_url] => https://512.png [banner_img_url] => [thumbnail_img_url] => [created_date] => 23132 [last_modified_date] => 2311 [last_updated_by] => 2134242 )
     * )
     * 
     * Output for this input array will be:
     * 
     * 
     * 
     */
    public static function buildKeyIndexedMap($arrObj, $indexKey, $getSingle = FALSE) {

        $resultMap = array();

        foreach ($arrObj as $key => $row) {
            //the row can be of any object type, so typecasting them to generic array
            $rowArr = (array) $row;

            if (empty($rowArr[$indexKey])) {
                //the index key is not present, so return empty map
                return $resultMap;
            }

            $attributeValue = $rowArr[$indexKey];

            if ($getSingle) {
                //if need to return to just single array and not list of value on the index, then remove nesting
                $resultMap[$attributeValue] = $rowArr;
                return $resultMap;
            }

            if (empty($resultMap[$attributeValue])) {
                $resultMap[$attributeValue] = array();
            }

            $listArr = $resultMap[$attributeValue];

            array_push($listArr, $rowArr);

            $resultMap[$attributeValue] = $listArr;
        }//foreach
        //debug
        //LassiDebugUtils::print_array ($resultMap, "indexed map");
        //return response. 
        return $resultMap;
    }

//function 

    /**
     * 
     * Utility method that tries to find if an element key exist in an array map or not
     * If, not exist then sets null, otherwise return the value of key in the array map
     * 
     * @param type $obj
     * @param type $param
     * @return string value
     */
    public static function setIfNotNull($obj, $param) {
        if (empty($obj[$param])) {
            return NULL;
        }

        return $obj[$param];
    }

//function

    /**
     * 
     * Utility method that tries to find if an element is null or not
     * If, null or empty then sets value to zero, otherwise return the value of element as it is
     * 
     * @param type $param
     * @return int value
     */
    public static function setZeroIfNull($param) {
        if ($param == NULL || empty($param)) {
            return 0;
        }

        return $param;
    }

//function

    /**
     * utility method that tries to set $key => $value on the given $arr array 
     * only if the $value is not empty
     * 
     * @param array $arr
     * @param type $value
     * @param type $key
     * @return array input array with the key => value set if the value is not null
     */
    public static function setElementIfNotNull($arr, $value, $key) {
        if (empty($value)) {
            return $arr;
        }

        $arr[$key] = $value;

        return $arr;
    }

//function

    /**
     * converts date string into epoch / unix timestamp
     * 
     * @param type $dateStr
     * @return epoch in INT
     */
    public static function getEpochForDateString($dateStr) {
        $epoch = strtotime($dateStr);
        return $epoch;
    }

//funtion

    /**
     * 
     * input is a list of array or array of arrays.
     * 
     * Checks if the inner array obj $field has the $fieldValue.
     * If not, then return false boolean.
     * 
     * @param type $arr
     * @param type $field
     * @param type $fieldValue
     */
    public static function checkArrayListForFieldValue($arrList, $field, $fieldValue) {
        foreach ($arrList as $key => $arr) {
            if (strcmp($arr[$fielf], $fieldValue) != 0) {
                return FALSE;
            }
        }

        return TRUE;
    }

//function

    /**
     * Query result usually returns a result object such as this 
     * 
     * Array
      (
      [0] => Array
      (
      [item_tax_code] => STD_GST_TAX
      )

      [1] => Array
      (
      [item_tax_code] => LIQ_GST_TAX
      )

      )
     * 
     * We need to convert this into a flattened list such as 
     * 
     * array ["STD_GST_TAX","LIQ_GST_TAX"]
     * 
     * 
     * This utility function is to help achieve this
     * 
     * @param type $queryResult
     * @param type $attribKey
     * @return array - flattened array
     */
    public static function flattenQueryResultByKey($queryResult, $attribKey) {
        $resultMap = array();

        foreach ($queryResult as $key => $rowObj) {

            $value = $rowObj [$attribKey];

            array_push($resultMap, $value);
        }

        return $resultMap;
    }

//function

    /**
     * Sample Input:
     * Array ( 
     * [0] => Array ( [primary_category] => SMALL BITE – VEG [tag_1] => TEST1 [tag_2] => [tag_3] => ) 
     * [1] => Array ( [primary_category] => SMALL BITE – VEG [tag_1] => [tag_2] => [tag_3] => ) 
     * [2] => Array ( [primary_category] => SMALL BITE – VEG [tag_1] => [tag_2] => TEST2 [tag_3] => ) 
     * [3] => Array ( [primary_category] => SMALL BITE – VEG [tag_1] => [tag_2] => [tag_3] => TEST3) 
     * )
     * 
     * The above is a standard query result object got from doing get all ->all() on Yii Query 
     * 
     * We want to transform and pick the unique values from this query result.
     * 
     * The Output for the above sample input would be:
     * 
     * Array ([0] => SMALL BITE - VEG, [1] => TEST1, [2] => TEST2, [3] => TEST3)
     * 
     * @param type $queryResult
     */
    public static function getDistinctValues($queryResult) {

        $distinctValues = array();

        foreach ($queryResult as $index => $rowObj) {
            foreach ($rowObj as $key => $value) {
                if (empty($value) OR (strcasecmp($value, "null") == 0)) {
                    continue;
                }
                //store all as upper case
                $distinctValues[$value] = true;
            }
        }

        return array_keys($distinctValues);
    }

//funct

    /**
     * 
     * Sample Input:
     * Array
      (
      [0] => Array
      (
      [category_name] => Beverages
      [img_url] => https://lassi-app-restaurant-images.sgp1.digitaloceanspaces.com/Categoriesimages/beverages.jpg
      )

      [1] => Array
      (
      [category_name] => Snacks
      [img_url] => https://lassi-app-restaurant-images.sgp1.digitaloceanspaces.com/Categoriesimages/snacks.jpg
      )

      [2] => Array
      (
      [category_name] => Dosa Varieties
      [img_url] => https://lassi-app-restaurant-images.sgp1.digitaloceanspaces.com/Categoriesimages/breakfast-dosa-varities.jpg
      )

      )
     * 
     * We want to transform this query result array into a map of key value pairs as request.
     * For example, we can pass in key = 'category_name' and value as 'img_url' then, 
     * the output would be:
     * 
     * Array (
     * [Dosa Varieties] => https://lassi-app-restaurant-images.sgp1.digitaloceanspaces.com/Categoriesimages/breakfast-dosa-varities.jpg,
     * [Snacks] => https://lassi-app-restaurant-images.sgp1.digitaloceanspaces.com/Categoriesimages/snacks.jpg
     * [Beverages] = https://lassi-app-restaurant-images.sgp1.digitaloceanspaces.com/Categoriesimages/beverages.jpg
     * )
     * 
     * @param type $queryResult
     * @param type $key
     * @param type $value
     */
    public static function buildMapFromQueryResult($queryResult, $key, $value) {

        $resultMap = array();

        foreach ($queryResult as $index => $rowObj) {
            $keyVal = $rowObj[$key];

            $valueVal = $rowObj[$value];

            $resultMap[$keyVal] = $valueVal;
        }

        return $resultMap;
    }

//funct

    /**
     * 
     * Very similar to the previous function to build Map from query result but 
     * builds a list / array and also substitutes friendly key names 
     * 
     * @param type $queryResult
     * @param type $key
     * @param type $value
     * @return type
     */
    public static function buildArrayMapFromQueryResult($queryResult, $key, $value, $friendlyKeyName, $isCAPSKEY = false) {

        $resultMap = array();

        foreach ($queryResult as $index => $rowObj) {

            $keyVal = $rowObj[$key];

            $valueVal = $rowObj[$value];

            $arrObj = array();

            $arrObj[$friendlyKeyName] = $keyVal;
            $arrObj[$value] = $valueVal;

            if ($isCAPSKEY) {
                //if the Key has to be all CAPS then convert it to CAPS
                $resultMap[mb_strtoupper($keyVal)] = $arrObj;
            }
            else {
                $resultMap[$keyVal] = $arrObj;
            }
        }

        return $resultMap;
    }

//funct

    public static function getElementValuesFromArray($inputArr, $key) {
        $result = array();
        foreach ($inputArr as $arr) {
            $keyValue = $arr[$key];
            array_push($result, $keyValue);
        }

        return $result;
    }

//func

    /**
     * 
     * The $categoryList arraymap has the full list of categories for the restaurant.
     * The $categoryWithImages array-map has the list of categories that have valid images in DB.
     * 
     * Many time, not all categories have images for all category names, so add placeholder images. 
     * 
     * @param type $categoryWithImages
     * @param type $categoryList
     */
    public static function addPlaceHolderCategoryImages($categoryWithImages, $categoryList) {
        foreach ($categoryList as $key => $categoryName) {
            $upperCatName = mb_strtoupper($categoryName);

            //the category with image details is already present. So skip adding placeholder image
            if (!empty($categoryWithImages[$upperCatName]) OR ( strcasecmp($categoryName, "null") == 0)) {
                continue;
            }

            $placeHolderImgURL = "https://lassi-app-restaurant-images.sgp1.cdn.digitaloceanspaces.com/Categoriesimages/generic_category.jpg";

            $catEntry = array("name" => $categoryName, "img_url" => $placeHolderImgURL);

            $categoryWithImages[$upperCatName] = $catEntry;
        }//

        return $categoryWithImages;
    }

//func

    public static function buildErrorMessageForSaveOperation($errors) {
        $errMsg = "";

        foreach ($errors as $errArr) {
            foreach ($errArr as $errItem) {
                $errMsg .= "$errItem , ";
            }
        }
        Yii::error("ERROR in Save operation: $errMsg");
        return $errMsg;
    }

//end func
}

//class
?>