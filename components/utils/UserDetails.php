<?php
namespace app\components\utils;

use Yii;


class UserDetails{

  public static function initialize(){
        self::set_person_id(Yii::$app->user->identity->person_id);
        self::set_role_id(Yii::$app->user->identity->role_id);
        self::set_restaurant_group_id(Yii::$app->user->identity->restaurant_group_id);
        self::set_restaurant_id(Yii::$app->user->identity->restaurant_id);
        self::set_first_name(Yii::$app->user->identity->first_name);
        self::set_last_name(Yii::$app->user->identity->last_name);
  } //end func


public static function set_person_id($person_id){
    Yii::$app->session['person_id'] = $person_id;
}
public static function set_role_id($role_id){
    Yii::$app->session['role_id'] = $role_id;
}
public static function set_first_name($first_name){
    Yii::$app->session['first_name'] = $first_name;
}
public static function set_last_name($last_name){
    Yii::$app->session['last_name'] = $last_name;
}
public static function set_restaurant_group_id($restaurant_group_id){
    Yii::$app->session['restaurant_group_id'] = $restaurant_group_id;
}
public static function set_restaurant_id($restaurant_id){
    Yii::$app->session['restaurant_id'] = $restaurant_id;
}

public static function get_person_id() {
    return Yii::$app->session['person_id'];
}

public static function get_role_id(){
  return Yii::$app->session['role_id'];
}

public static function get_first_name(){
  return Yii::$app->session['first_name'];
}

public static function get_last_name(){
  return Yii::$app->session['last_name'];
}

public static function get_restaurant_group_id(){
  return Yii::$app->session['restaurant_group_id'];
}

public static function get_restaurant_id(){
  return Yii::$app->session['restaurant_id'];
}
}//class